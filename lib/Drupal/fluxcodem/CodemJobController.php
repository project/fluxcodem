<?php

/**
 * @file
 * Contains CodemJobController.
 */

namespace Drupal\fluxcodem;

use Drupal\fluxservice\Plugin\Entity\AccountInterface;
use Drupal\fluxservice\Plugin\Entity\ServiceInterface;
use Drupal\fluxservice\Entity\RemoteEntityInterface;
use Drupal\fluxservice\Entity\RemoteEntityControllerBySingleService;

/**
 * Entity controller for Codem jobs.
 */
class CodemJobController extends RemoteEntityControllerBySingleService {

  /**
   * {@inheritdoc}
   */
  protected function loadFromService($ids) {
    $output = array();
    $client = $this->getService()->client();
    foreach ($ids as $id) {
      if ($response = $client->getJob(array('id' => $id))) {
        $output[$id] = $response;
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected function sendToService(RemoteEntityInterface $job) {
    $client = $this->getService()->client();
    return $client->createJob(array('job' => $job));
  }

  /**
   * {@inheritdoc}
   */
  public function getService() {
    return entity_load_single('fluxservice_service', '978233a8-67b9-49b5-98af-17bcf16841bd');
  }

}
