<?php

/**
 * @file
 * Contains CodemCreateJobAction.
 */

namespace Drupal\fluxcodem\Plugin\Rules\Action;

/**
 * Create a codem job action.
 */
class CodemCreateJobAction extends CodemActionBase {
  /**
   * Defines the action.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxcodem_create_job',
      'label' => t('Create job'),
      'parameter' => array(
        'source_file' => array(
          'type' => 'text',
          'label' => t('Source file URL'),
          'default mode' => 'selector',
        ),
        'destination_file' => array(
          'type' => 'text',
          'label' => t('Optional destination file URL override'),
          'description' => t('If not specified, this will get automatically generated: @todo pattern'),
          'optional' => TRUE,
        ),
        'encoder_options' => array(
          'type' => 'text',
          'label' => t('Encoder options'),
          'description' => t('If not specified, this will get automatically generated: @todo show default value'),
          'optional' => TRUE,
          //todo: Move this to a constant or global config.
          'default_value' => '-acodec libfaac -ab 96k -ar 44100 -vcodec libx264 -vb 416k -s 320x180 -y -threads 0'
        ),
        'include_default_callback' => array(
          'type' => 'boolean',
          'label' => t('Include default callback for notifications'),
          'description' => t('If enabled, include a @url as a callback for codem notifications. This will automatically trigger the codem notification rules event.', array(
            '@url' => $GLOBALS['base_url'] . '/codem/notification',
          )),
          'optional' => TRUE,
          'default_value' => TRUE,
        ),
        //todo: Implement additional options.
        /*
        "thumbnail_options": {
        "location": "json",
          "type": "array",
          "percentages": {
          "type": "string"
          },
          "size": {
          "type": "string"
          },
          "format": {
          "type": "string"
          }
        },
        "callback_urls": {
        "location": "json",
          "type": "array"
        }
        */
      ),
      'provides' => array(
        'job_created' => array('type' => 'fluxcodem_job', 'label' => t('Job created')),
      ),
    );
  }

  /**
   * Executes the action.
   */
  public function execute($source_file, $destination_file, $encoder_options, $include_default_callback) {
    if (empty($destination_file)) {
      $destination_file = $source_file . '_test.video';
    }
    if (empty($encoder_options)) {
      $encoder_options = '-acodec libfaac -ab 96k -ar 44100 -vcodec libx264 -vb 416k -s 320x180 -y -threads 0';
    }

    $callback_urls = array(
    );

    if ($include_default_callback) {
      $callback_urls += array(
        $GLOBALS['base_url'] . '/codem/notification'
      );
    }

    // Prepare request values.
    $values = array(
      'source_file' => $source_file,
      'destination_file' => $destination_file,
      'encoder_options' => $encoder_options,
      'callback_urls' => $callback_urls,
    );

    // Send create job request.
    $service = $this->service();
    $client = $service->client();
    //$response = $client->createJob(array("body" => "test"));
    $response = $client->createJob($values);

    return array(
      'job_created' => fluxservice_bycatch((array) $response, 'fluxcodem_job', $service)
    );
  }
}
