<?php

/**
 * @file
 * Contains CodemGetJobAction.
 */

namespace Drupal\fluxcodem\Plugin\Rules\Action;

/**
 * Get a single job action.
 */
class CodemGetJobAction extends CodemActionBase {
  /**
   * Defines the action.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxcodem_get_job',
      'label' => t('Get job'),
      'parameter' => array(
        'job_id' => array(
          'type' => 'text',
          'label' => t('Job id'),
          'default mode' => 'selector',
        ),
      ),
      'provides' => array(
        'job' => array('type' => 'fluxcodem_job', 'label' => t('Job item')),
      ),
    );
  }

  /**
   * Executes the action.
   */
  public function execute($job_id) {
    // Send get jobs request.
    $service = $this->service();
    $client = $service->client();
    $response = $client->getJob(array('job_id' => $job_id));

    return array(
      'job' => fluxservice_bycatch((array) $response, 'fluxcodem_job', $service)
    );
  }
}
