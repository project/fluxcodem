<?php

/**
 * @file
 * Contains CodemActionBase.
 */

namespace Drupal\fluxcodem\Plugin\Rules\Action;

use Drupal\fluxservice\Rules\FluxRulesPluginHandlerBase;

/**
 * Base class for codem Rules plugin handler.
 */
abstract class CodemActionBase extends FluxRulesPluginHandlerBase implements \RulesActionHandlerInterface {

  /**
   * Returns info-defaults for codem plugin handlers.
   */
  public static function getInfoDefaults() {
    return array(
      'category' => 'fluxcodem',
      'access callback' => array(get_called_class(), 'integrationAccess'),
    );
  }

  /**
   * Rules codem integration access callback.
   */
  public static function integrationAccess($type, $name) {
    return fluxservice_access_by_plugin('fluxcodem');
  }

  /**
   * Returns info suiting for codem service account parameters.
   */
  public static function getServiceParameterInfo() {
    return array();
  }

  /**
   * Helper function to get the service.
   *
   * @return CodemService
   */
  protected function service() {
    return entity_get_controller('fluxcodem_job')->getService();
  }

}
