<?php

/**
 * @file
 * Contains CodemGetJobsAction.
 */

namespace Drupal\fluxcodem\Plugin\Rules\Action;

/**
 * Get a list of jobs action.
 */
class CodemGetJobsAction extends CodemActionBase {
  /**
   * Defines the action.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxcodem_get_jobs',
      'label' => t('Get jobs'),
      'parameter' => array(
      ),
      'provides' => array(
        'jobs' => array('type' => 'list<fluxcodem_job>', 'label' => t('Job list')),
        'max_slots' => array('type' => 'integer', 'label' => t('Max slots')),
        'free_slots' => array('type' => 'integer', 'label' => t('Free slots')),
      ),
    );
  }

  /**
   * Executes the action.
   */
  public function execute() {
    // Send get jobs request.
    $service = $this->service();
    $client = $service->client();
    $response = $client->getJobs();

    return array(
      'job_list' => fluxservice_bycatch_multiple((array) $response['jobs'], 'fluxcodem_job', $service),
      'max_slots' => $response['max_slots'],
      'free_slots' => $response['max_slots'],
    );
  }
}
