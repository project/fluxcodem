<?php

/**
 * @file
 * Contains TwitterEventBase.
 */

namespace Drupal\fluxcodem\Plugin\Rules\EventHandler;

use Drupal\fluxcodem\Plugin\Rules\Action\CodemActionBase;

/**
 * Cron-based base class for Twitter event handlers.
 */
abstract class CodemEventBase extends \RulesEventHandlerBase {

  /**
   * Returns info-defaults for twitter plugin handlers.
   */
  public static function getInfoDefaults() {
    return CodemActionBase::getInfoDefaults();
  }

  /**
   * Rules twitter integration access callback.
   */
  public static function integrationAccess($type, $name) {
    return fluxservice_access_by_plugin('fluxcodem');
  }

  /**
   * Returns info for the provided twitter service account variable.
   */
  public static function getServiceVariableInfo() {
    return array(
      'type' => 'fluxservice_service',
      'bundle' => 'fluxcodem',
      'label' => t('Service'),
      'description' => t('The codem service used.'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaults() {
    return array(
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form_state) {
    $settings = $this->getSettings();

    // todo: Implement form?
    $form = array();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventNameSuffix() {
    return drupal_hash_base64(serialize($this->getSettings()));
  }

}
