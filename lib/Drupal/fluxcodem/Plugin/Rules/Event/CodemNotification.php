<?php

/**
 * @file
 * Contains TwitterHomeTimelineEvent.
 */

namespace Drupal\fluxcodem\Plugin\Rules\EventHandler;

/**
 * Event handler for tweets on the personal timeline.
 */
class CodemNotification extends CodemEventBase {

  /**
   * Defines the event.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxcodem_notification',
      'label' => t('A codem notification has been received'),
      'variables' => array(
        'id' => array(
          'label' => 'Job id',
          'type' => 'text',
        ),
        'status' => array(
          'label' => 'Status',
          'type' => 'text',
        ),
        'job' => array(
          'label' => 'The updated job',
          'type' => 'fluxcodem_job',
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $settings = $this->getSettings();
    /*
    if ($settings['account'] && $account = entity_load_single('fluxservice_account', $settings['account'])) {
      return t('A new Tweet appears on the home timeline of %account.', array('%account' => "@{$account->label()}"));
    }
    */
    return $this->eventInfo['label'];
  }

}
