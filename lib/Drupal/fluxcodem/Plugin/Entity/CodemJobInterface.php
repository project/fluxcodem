<?php

/**
 * @file
 * Contains CodemJobInterface.
 */

namespace Drupal\fluxcodem\Plugin\Entity;

use Drupal\fluxservice\Entity\RemoteEntityInterface;

/**
 * Interfaces for Tweet objects.
 */
interface CodemJobInterface extends RemoteEntityInterface {

}
