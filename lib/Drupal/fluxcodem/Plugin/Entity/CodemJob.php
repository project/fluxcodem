<?php

/**
 * @file
 * Contains CodemJob.
 */

namespace Drupal\fluxcodem\Plugin\Entity;

use Drupal\fluxservice\Entity\RemoteEntity;

/**
 * Entity class for Codem Jobs.
 */
class CodemJob extends RemoteEntity implements CodemJobInterface {

  /**
   * Defines the entity type.
   *
   * This gets exposed to hook_entity_info() via fluxservice_entity_info().
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxcodem_job',
      'label' => t('Codem: Job'),
      'module' => 'fluxcodem',
      'service' => 'fluxcodem',
      'controller class' => '\Drupal\fluxcodem\CodemJobController',
      'entity keys' => array(
        'id' => 'drupal_entity_id',
        'remote id' => 'id',
      ),
      /*
      'fluxservice_efq_driver' => array(
        'default' => '\Drupal\fluxcodem\CodemJobEntityQueryDriver',
      ),
      */
    );
  }

  /**
   * Gets the entity property definitions.
   */
  public static function getEntityPropertyInfo($entity_type, $entity_info) {
    $info['id'] = array(
      'label' => t('Remote identifier'),
      'description' => t('The unique remote identifier of the Job.'),
      'type' => 'integer',
    );

    $info['source_file'] = array(
      'label' => t('Source file path'),
      'type' => 'text',
    );

    $info['destination_file'] = array(
      'label' => t('Desination file path'),
      'type' => 'text',
    );

    $info['status'] = array(
      'label' => t('Status'),
      'type' => 'text',
    );

    $info['progress'] = array(
      'label' => t('Progress'),
      'type' => 'decimal',
    );

    $info['duration'] = array(
      'label' => t('Duration'),
      'type' => 'integer',
    );

    $info['filesize'] = array(
      'label' => t('Filesize'),
      'type' => 'integer',
    );

    $info['message'] = array(
      'label' => t('Message'),
      'type' => 'text',
    );

    $info['created_at'] = array(
      'label' => t('Created at'),
      'type' => 'date',
    );

    $info['updated_at'] = array(
      'label' => t('Updated at'),
      'type' => 'date',
    );

    // Specifically for notifications.
    $info['state'] = array(
      'label' => t('State'),
      'type' => 'text',
    );
    $info['callback_url'] = array(
      'label' => t('Callback url'),
      'type' => 'uri',
    );
    $info['completed_at'] = array(
      'label' => t('Completed at'),
      'type' => 'date',
    );
    $info['host_id'] = array(
      'label' => t('Host id'),
      'type' => 'integer',
    );
    $info['preset_id'] = array(
      'label' => t('Preset id'),
      'type' => 'integer',
    );
    $info['transcoding_started_at'] = array(
      'label' => t('Transcoding started at'),
      'type' => 'date',
    );

    return $info;
  }

}
