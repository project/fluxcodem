<?php

/**
 * @file
 * Contains CodemService.
 */

namespace Drupal\fluxcodem\Plugin\Service;

use Drupal\fluxservice\Plugin\Entity\Service;
use Drupal\fluxcodem\CodemClient;
/**
 * Service plugin implementation for Codem.
 */
class CodemService extends Service implements CodemServiceInterface {

  /**
   * Defines the plugin.
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxcodem',
      'label' => t('Codem'),
      'description' => t('Provides Codem integration for fluxkraft.'),
      'icon font class' => 'icon-codem',
      'icon background color' => '#2fa8fc'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return parent::getDefaultSettings() + array(
      'base_url' => 'http://localhost:8080',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array &$form_state) {
    $form = parent::settingsForm($form_state);

    $form['help'] = array(
      '#type' => 'markup',
      '#markup' => t('This is some general help text.'),
      '#prefix' => '<p class="fluxservice-help">',
      '#suffix' => '</p>',
      '#weight' => -1,
    );

    $form['base_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Codem service base url'),
      '#description' => t('Your codem server url.'),
      '#default_value' => 'http://localhost:8080',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function client() {
    return CodemClient::factory($this->data->toArray());
  }

}
