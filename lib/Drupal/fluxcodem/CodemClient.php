<?php

/**
 * @file
 * Contains CodemClient.
 */

namespace Drupal\fluxcodem;

use Drupal\fluxservice\ServiceClientInterface;
use Guzzle\Common\Collection;
use Guzzle\Service\Client;
use Guzzle\Service\Description\ServiceDescription;

/**
 * Guzzle driven service client for the Codem API.
 */
class CodemClient extends Client {

  /**
   * {@inheritdoc}
   */
  public static function factory($config = array()) {
    $required = array('base_url');
    $defaults = array(
      'base_url' => 'https://mycodemurl',
    );

    $config = Collection::fromConfig($config, $defaults, $required);
    $client = new static($config->get('base_url'), $config);

    // Attach a service description to the client
    $description = ServiceDescription::factory(__DIR__ . '/codem.json');
    $client->setDescription($description);

    return $client;
  }

}
