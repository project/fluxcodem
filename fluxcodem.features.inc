<?php
/**
 * @file
 * wvautoverbrauch.features.inc
 */

/**
 * Implements hook_views_api().
 */
function fluxcodem_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_fluxservice_service().
 */
function fluxcodem_default_fluxservice_service() {
  $items = array();
  $items['978233a8-67b9-49b5-98af-17bcf16841bd'] = entity_import('fluxservice_service', '{
    "uuid" : "978233a8-67b9-49b5-98af-17bcf16841bd",
    "label" : "Codem job service",
    "data" : {},
    "plugin" : "fluxcodem",
    "uid" : null,
    "rdf_mapping" : []
  }');
  return $items;
}
